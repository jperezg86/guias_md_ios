//
//  ViewController.swift
//  Guías de México
//
//  Created by Jose Manuel Perez on 10/23/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import MaterialComponents

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionViewAvailableGuides: UICollectionView!
    var guideAvailables : [Guia]  = [Guia]()
    var indexPathArray = [IndexPath]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        self.downloadGuiasAvailables()
        self.collectionViewAvailableGuides.delegate = self
        self.collectionViewAvailableGuides.dataSource = self
        let nibCell = UINib(nibName: "PortadaGuiaCell", bundle: nil)
        self.collectionViewAvailableGuides.register(nibCell, forCellWithReuseIdentifier: "cell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated )
        let width = (collectionViewAvailableGuides.frame.width - 10) / 2
        let height = (150 * width) / 100;
        let layout = collectionViewAvailableGuides.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
    }
    
    
    private func setupNavigationBar(){
        let image = UIImage(named: "logo-md")?.imageWithInsets(insets: UIEdgeInsets(top: 40, left: 0, bottom: 40, right: 0))
        let logoImageView = UIImageView(image: image)
        logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        logoImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = logoImageView
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.guideAvailables.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PortadaGuiaCell
        if !indexPathArray.contains(indexPath) {
            indexPathArray.append(indexPath)
            let guia = self.guideAvailables[indexPath.item]
            
            cell.lblTituloGuia.text = guia.title
            cell.imagePortada.pin_setImage(from: URL(string: guia.featuredMedia.medium.sourceUrl))
            cell.layer.masksToBounds = false;
            cell.setShadowElevation(ShadowElevation(rawValue: 8), for: .highlighted)
            cell.setShadowColor(UIColor.black, for: .highlighted)
        }
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let guiaSelected = self.guideAvailables[indexPath.item]
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let resultViewController = storyBoard.instantiateViewController(withIdentifier: "vc_destinos") as! DestinosViewController
        self.navigationController?.pushViewController(resultViewController, animated: true)
        resultViewController.setGuide(guide: guiaSelected)
    }
    
    func downloadGuiasAvailables(){
        let urlGuias = "https://guiasdigitales.wpengine.com/wp-json/wp/v2/pages?&_embed&status=publish&categories=2&orderby=date&order=asc&available=1"
        guard let url = URL(string: urlGuias) else {return}
        
        URLSession.shared.dataTask(with: url) {
            (data,response,err) in
            guard let data = data else { return }
            do{
                self.guideAvailables = try
                    JSONDecoder().decode([Guia].self, from: data)
                DispatchQueue.main.sync {
                    self.collectionViewAvailableGuides.reloadData()
                }
            }catch let jsonErr {
                print(jsonErr)
            }
        }.resume()
        
    }
}

