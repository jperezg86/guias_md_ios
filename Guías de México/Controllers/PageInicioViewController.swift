//
//  PageInicioViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/8/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import MaterialComponents

class PageInicioViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    
    var destino : Destino!
    var posts : [String : [Post]]!
    var indexPathDictionary :  [String : [IndexPath]] = [String : [IndexPath]]()
    var delegate : PageInicioViewControllerDelegate!

    @IBOutlet weak var collectionViewAtractivos: UICollectionView!
    @IBOutlet weak var collectionViewActividades: UICollectionView!
    @IBOutlet weak var collectionViewDondeComer: UICollectionView!
    @IBOutlet weak var collectionViewDondeDormir: UICollectionView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.indexPathDictionary["actividades"] = [IndexPath]()
        self.indexPathDictionary["atractivos"] = [IndexPath]()
        self.indexPathDictionary["dormir"] = [IndexPath]()
        self.indexPathDictionary["comer"] = [IndexPath]()
        
        collectionViewAtractivos.delegate = self
        collectionViewActividades.delegate = self
        collectionViewDondeComer.delegate = self
        collectionViewDondeDormir.delegate = self
        
        collectionViewAtractivos.dataSource = self
        collectionViewActividades.dataSource = self
        collectionViewDondeComer.dataSource = self
        collectionViewDondeDormir.dataSource = self
        
        let nibCell = UINib(nibName: "CarouselItemCollectionViewCell", bundle: nil)
        
        self.collectionViewActividades.register(nibCell, forCellWithReuseIdentifier: "cell")
        self.collectionViewAtractivos.register(nibCell, forCellWithReuseIdentifier: "cell")
        self.collectionViewDondeComer.register(nibCell, forCellWithReuseIdentifier: "cell")
        self.collectionViewDondeDormir.register(nibCell, forCellWithReuseIdentifier: "cell")
        
        // Do any additional setup after loading the view.
    }
    
    
    func setPosts(posts : [String : [Post]] ){
        self.posts = posts
//        self.collectionViewActividades.reloadData()
//        self.collectionViewAtractivos.reloadData()
//        self.collectionViewDondeDormir.reloadData()
//        self.collectionViewDondeComer.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if((self.posts) != nil){
            if(collectionView == collectionViewAtractivos){
                return (self.posts["atractivos"]?.count)!
            }else if(collectionView == collectionViewActividades){
                return (self.posts["actividades"]?.count)!
            }else if(collectionView == collectionViewDondeDormir){
                return (self.posts["dormir"]?.count)!
            }else{
                return (self.posts["comer"]?.count)!
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CarouselItemCollectionViewCell
        let indexPathArray : [IndexPath]!
        
        if((self.posts) != nil){
            let postsTmp : [Post]!
            if(collectionView == collectionViewAtractivos){
                postsTmp = self.posts["atractivos"]
                indexPathArray = self.indexPathDictionary["atractivos"]
            }else if(collectionView == collectionViewActividades){
                postsTmp = self.posts["actividades"]
                indexPathArray = self.indexPathDictionary["actividades"]
            }else if(collectionView == collectionViewDondeDormir){
                postsTmp = self.posts["dormir"]
                indexPathArray = self.indexPathDictionary["dormir"]
            }else{
               postsTmp = self.posts["comer"]
                indexPathArray = self.indexPathDictionary["comer"]
            }
            cell.title.text = postsTmp[indexPath.item].title
            
            if let encodedString = postsTmp[indexPath.item].featuredMedia.full.sourceUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encodedString)
            {
                cell.image.pin_setImage(from: url)
            }
            
            
            if !indexPathArray.contains(indexPath) {
                indexPathArray.append(indexPath)
                cell.layer.masksToBounds = false;
                cell.setShadowElevation(ShadowElevation(rawValue: 5), for: .normal)
                cell.setShadowColor(UIColor.black, for: .normal)
                cell.cornerRadius = 0.0
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if((self.posts) != nil){
            let postsTmp : [Post]!
            if(collectionView == collectionViewAtractivos){
                postsTmp = self.posts["atractivos"]
            }else if(collectionView == collectionViewActividades){
                postsTmp = self.posts["actividades"]
            }else if(collectionView == collectionViewDondeDormir){
                postsTmp = self.posts["dormir"]
            }else{
                postsTmp = self.posts["comer"]
            }
            self.delegate.pageInicioOnItemClicked(post: postsTmp[indexPath.item])
            
//            if !indexPathArray.contains(indexPath) {
//                indexPathArray.append(indexPath)
//                cell.layer.masksToBounds = false;
//                cell.setShadowElevation(ShadowElevation(rawValue: 5), for: .normal)
//                cell.setShadowColor(UIColor.black, for: .normal)
//                cell.cornerRadius = 0.0
//            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


protocol PageInicioViewControllerDelegate{
    func pageInicioOnItemClicked(post : Post)
}
