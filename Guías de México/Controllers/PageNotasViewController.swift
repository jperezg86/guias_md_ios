//
//  MapNotasViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/9/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

import MaterialComponents

protocol PageNotasViewControllerDelegate {
    func onViewReady(controller : PageNotasViewController)
}

class PageNotasViewController : UIViewController, MapNotasViewControllerDelegate, PageInicioViewControllerDelegate {
    
    var index : Int!
    var notas : [Post]!
    var delegate : PageNotasViewControllerDelegate!
    var bottomSheet : MDCBottomSheetController!
    var mapViewController : MapNotasViewController!
    var destino : Destino!
    var guia : Guia!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.delegate != nil {
            self.delegate.onViewReady(controller: self)
        }
        
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        if(self.bottomSheet != nil){
            present(self.bottomSheet, animated: false)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mapNotasViewControllerSegue" {
            self.mapViewController = segue.destination as? MapNotasViewController
            self.mapViewController.delegate = self
        }
    }
    
//    override func performSegue(withIdentifier identifier: String, sender: Any?) {
//
//    }

    
    func setupBottomSheet(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        if (self.index == 0){
            let bottomSheetViewController = storyBoard.instantiateViewController(withIdentifier: "vc_page_inicio") as? PageInicioViewController
            bottomSheetViewController!.destino = self.destino
            bottomSheetViewController?.delegate = self
            var groupedPosts : [String : [Post]] = [String:[Post]]()
            groupedPosts["actividades"] = self.searchPosts(from: self.notas, byCategory: Constants.POST_CATEGORY_ACTIVIDADES)
            groupedPosts["atractivos"] = self.searchPosts(from: self.notas, byCategory: Constants.POST_CATEGORY_ATRACTIVOS)
            groupedPosts["comer"] = self.searchPosts(from: self.notas, byCategory: Constants.POST_CATEGORY_COMER)
            groupedPosts["dormir"] = searchPosts(from: self.notas, byCategory: Constants.POST_CATEGORY_DORMIR)
            bottomSheetViewController?.setPosts(posts: groupedPosts)
            self.bottomSheet = MDCBottomSheetController(contentViewController: bottomSheetViewController!)
            self.bottomSheet.hidesBottomBarWhenPushed = false
            self.bottomSheet.isScrimAccessibilityElement = true
            present(self.bottomSheet, animated: true)
        }else{
            let bottomSheetViewController = storyBoard.instantiateViewController(withIdentifier: "tableview_notas") as? NotasTableViewController
            bottomSheetViewController?.setNotas(notas: self.notas)
            self.bottomSheet = MDCBottomSheetController(contentViewController: bottomSheetViewController!)
            self.bottomSheet.hidesBottomBarWhenPushed = false
            self.bottomSheet.isScrimAccessibilityElement = true
            present(self.bottomSheet, animated: true)
        }
    }
    
    /**
     * Devuelve los items Post que encajan en la categoría indicada por aguja
     * de una lista de post original (pajar).
     * @param pajar La lista de elementos donde va a buscar
     * @param aguja El id de la categoría a buscar
     * @return La lista de post que coinciden con esa categoría
     */
    func searchPosts(from pajar : [Post], byCategory category: Int) -> [Post]{
        var postEncontrados : [Post] = [Post]()
        for post : Post in pajar {
            if (post.categories.firstIndex(of: category) != nil) {
                postEncontrados.append(post)
            }
        }
        return postEncontrados
    }
    
    func onMapReady(controller: MapNotasViewController) {
        controller.notas = self.notas
        controller.buildMapa()
    }
    
    
    func pageInicioOnItemClicked(post: Post) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let postsViewController = storyBoard.instantiateViewController(withIdentifier: "vc_post") as? PostViewController
        self.navigationController?.pushViewController(postsViewController!, animated: true)
        postsViewController?.setNota(nota: post, withDestino: self.destino, fromGuia: self.guia)
    }
    
    
}
