//
//  FullScreenViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/22/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class FullScreenViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    
    @IBOutlet weak var collectionViewGallery: UICollectionView!
    var galleryItems : [GalleryItem]!
    var currentPage : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //"fullscreen_image_cell"
        self.collectionViewGallery.delegate = self
        self.collectionViewGallery.dataSource = self
        let nibCell = UINib(nibName: "FullScreenGalleryCollectionViewCell", bundle: nil)
        self.collectionViewGallery.register(nibCell, forCellWithReuseIdentifier: "fullscreen_image_cell")
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated )
        let width = UIScreen.main.bounds.width
        let height = collectionViewGallery.frame.height
        let layout = collectionViewGallery.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.galleryItems != nil){
            return self.galleryItems.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fullscreen_image_cell", for: indexPath) as! FullScreenGalleryCollectionViewCell
        if (self.galleryItems != nil){
            let image = self.galleryItems[indexPath.item]
            if let encodedString = image.url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encodedString)
            {
                cell.image.pin_setImage(from: url)
                cell.tag = indexPath.item
            }
            cell.caption.text = image.description
        }
        return cell;
    }
    
    
    @IBAction func onForwardPressed(_ sender: Any) {
        if(currentPage < self.galleryItems.count){
            let pageWidth = self.collectionViewGallery.frame.size.width
            let scrollTo =  CGPoint(x: (pageWidth * CGFloat(self.currentPage)), y: 0)
            self.collectionViewGallery.setContentOffset(scrollTo, animated: true)
            currentPage+=1
        }
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        if(currentPage > 0){
            currentPage-=1
            let pageWidth = self.collectionViewGallery.frame.size.width
            let scrollTo =  CGPoint(x: (pageWidth * CGFloat(self.currentPage)), y: 0)
            self.collectionViewGallery.setContentOffset(scrollTo, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = self.collectionViewGallery.frame.size.width;
        self.currentPage = Int(self.collectionViewGallery.contentOffset.x / pageWidth)
        print(self.currentPage)
    }
    
    
    func setImages( images : [GalleryItem]){
        self.galleryItems = images
        self.collectionViewGallery.reloadData()
    }
    
    @IBAction func close(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
