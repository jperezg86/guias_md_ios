//
//  MapViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 10/31/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    var destinos : [Destino]!
    var mapView : GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: 19.436351, longitude: -99.1814577, zoom: 5.0)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.view = self.mapView
        
        // Do any additional setup after loading the view.
    }
    
    
    func setDestinos(destinos : [Destino]){
        self.destinos = destinos
        
        for destino in destinos{
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: destino.ubicacion.lat, longitude: destino.ubicacion.lng)
            marker.title = destino.title
            marker.icon = UIImage(named: "pin")
            marker.snippet = destino.title
            marker.groundAnchor = CGPoint(x: 0.5, y: 1)
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.map = self.mapView
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
