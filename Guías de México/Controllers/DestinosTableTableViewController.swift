//
//  DestinosTableTableViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 10/26/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit



class DestinosTableTableViewController: UITableViewController {
    
    var guia : Guia!
    
    var delegate : DestinosTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "DestinosTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "destino_cell")
        
        self.tableView.rowHeight = 120
        
        let headerView = (Bundle.main.loadNibNamed("CustomHeaderCell", owner: self, options: nil)![0] as? UIView)
        tableView.tableHeaderView = headerView
    }
    
    public func setGuia(guide : Guia){
        self.guia = guide
        self.tableView.reloadData()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.guia != nil) ? self.guia.destinos.count : 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "destino_cell", for: indexPath) as! DestinosTableViewCell
        if(self.guia != nil){
            let destino = self.guia.destinos[indexPath.item]
            cell.imagenDesto.pin_setImage(from: URL(string: destino.featuredMedia.full.sourceUrl))
            if let encodedString = destino.featuredMedia.full.sourceUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                 let url = URL(string: encodedString)
            {
                cell.imagenDesto.pin_setImage(from: url)
            }
            cell.labelDestino.text = destino.title
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemSelected = self.guia.destinos[indexPath.item]
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didPressedItem(destino: itemSelected)
        print(itemSelected);
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0;
    }
}

protocol DestinosTableViewControllerDelegate {
    func didPressedItem(destino : Destino)
}
