//
//  PostViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/16/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import MaterialComponents
import Atributika
import Karte
import MapKit
import youtube_ios_player_helper

class PostViewController: UIViewController, PostMapViewControllerDelegate,
UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource
{
   
    
    @IBOutlet weak var videoPlayer: YTPlayerView!
    @IBOutlet weak var lblDestino: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet var cardViews: [MDCCard]!
    @IBOutlet weak var lblExcerpt: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblGenerales: UILabel!
    @IBOutlet weak var btnComoLlegar: UIButton!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tableViewMasDestinos: UITableView!
    @IBOutlet weak var galleryView: UIView!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    
    
    var currentPage : Int = 1
    var otrosDestinos : [Destino] = [Destino]()
    var guia : Guia!
    var destino : Destino!
    var nota : Post!
    var mapViewController : PostMapViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for cardView  in cardViews {
            cardView.setShadowElevation(ShadowElevation(rawValue: 5.0), for: .normal)
//            cardView.isEnabled = false
        }
        
        self.tableViewMasDestinos.delegate = self
        self.tableViewMasDestinos.dataSource = self

        let nib = UINib(nibName: "DestinosTableViewCell", bundle: nil)
        self.tableViewMasDestinos.register(nib, forCellReuseIdentifier: "destino_cell")
        self.tableViewMasDestinos.rowHeight = 120
        
        self.galleryCollectionView.delegate = self
        self.galleryCollectionView.dataSource = self
        
        let nibCell = UINib(nibName: "GalleryCollectionViewCell", bundle: nil)
        self.galleryCollectionView.register(nibCell, forCellWithReuseIdentifier: "image_cell")
        
        self.selectOtherDestinos()
        self.dataBinding()
        
        setupNavigationBar()
    }
    
    
    func setupNavigationBar(){
        if(!self.guia.logoAlternativo.isEmpty){
            if let encodedString = self.guia.logoAlternativo.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encodedString)
            {
                let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 64))
                //                logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                logoImageView.pin_setImage(from: url)
                logoImageView.contentMode = .scaleAspectFit
                navigationItem.titleView = logoImageView
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated )
        let width = UIScreen.main.bounds.width
        let height = galleryCollectionView.frame.height
        let layout = galleryCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
        
    }
    
    func selectOtherDestinos(){
        repeat {
            let randomNumber = arc4random_uniform(UInt32(self.guia.destinos.count - 1))
            let destinoTmp = self.guia.destinos[Int(randomNumber)]
            if(destinoTmp.id != self.destino.id){
                if !self.otrosDestinos.contains(where: {$0.id == destinoTmp.id}) {
                        self.otrosDestinos.append(destinoTmp)
                }
            }
        }while(self.otrosDestinos.count < 3)
        self.tableViewMasDestinos.reloadData()
    }
    
    func setNota(nota : Post, withDestino destino : Destino, fromGuia : Guia){
        self.nota = nota
        self.destino = destino
        self.guia = fromGuia
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "post_map_view_controller" {
            self.mapViewController = segue.destination as? PostMapViewController
            self.mapViewController.delegate = self
        }
    }
    
    func dataBinding(){
        if(self.nota.gallery == nil){
            self.galleryView.visiblity(gone: true)
        }
        self.lblDestino.text = self.destino.title
        self.lblTitle.text = self.nota.title
        if let encodedString = self.nota.featuredMedia.full.sourceUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encodedString)
        {
            self.postImage.pin_setImage(from: url)
        }
        
        self.lblCategory.text = self.nota.getCategoryAsString()
        self.lblExcerpt.text = self.nota.excerpt
        let font = UIFont(name: "Montserrat-Regular", size: 16)!
        let links = Style.foregroundColor(.blue)
        let phoneNumbers = Style.backgroundColor(.yellow)
        let headers = Style("h3").font(UIFont(name: "Montserrat-SemiBold", size: 18)!)
        let mentions = Style.font(.italicSystemFont(ofSize: 12)).foregroundColor(.black)
        let strong = Style("strong").font(UIFont(name: "Montserrat-SemiBold", size: 16)!).foregroundColor(UIColor(rgb: 0xdf0063))
        let all = Style.font(font).foregroundColor(.black)
        let cardVideo = self.cardViews.first(where: {$0.tag == 2})
        
        if(self.nota.videoCode.isEmpty){
           cardVideo?.visiblity(gone: true)
        }else{
            self.videoPlayer.load(withVideoId: self.nota.videoCode)
        }
        
        let str = self.nota.body
            .style(tags: strong, headers)
            .styleMentions(mentions)
            .styleHashtags(links)
            .styleLinks(links)
            .stylePhoneNumbers(phoneNumbers)
            .styleAll(all).attributedString

        self.lblContent.attributedText = str
        self.lblGenerales.attributedText = self.nota.generales.style(tags: strong,headers).styleAll(all).attributedString
    }
    
    func onMapReady(controller: PostMapViewController) {
        if(self.nota.ubicacion != nil){
            controller.ubicacion = self.nota.ubicacion
            controller.buildMapa()
        }else{
            self.mapView.visiblity(gone: true)
            self.btnComoLlegar.visiblity(gone: true)
        }
        
    }
    
    
    @IBAction func showRoute(_ sender: Any) {
        let coordinate = CLLocationCoordinate2D(latitude: self.nota.ubicacion.lat, longitude: self.nota.ubicacion.lng)
        
        let point = Location(name: self.nota.title, coordinate: coordinate)
        Karte.presentPicker(destination: point, presentOn: self)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        if(currentPage < self.nota.gallery.count){
            let pageWidth = self.galleryCollectionView.frame.size.width
            let scrollTo =  CGPoint(x: (pageWidth * CGFloat(self.currentPage)), y: 0)
            self.galleryCollectionView.setContentOffset(scrollTo, animated: true)
            currentPage+=1
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        if(currentPage > 0){
            currentPage-=1
            let pageWidth = self.galleryCollectionView.frame.size.width
            let scrollTo =  CGPoint(x: (pageWidth * CGFloat(self.currentPage)), y: 0)
            self.galleryCollectionView.setContentOffset(scrollTo, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.otrosDestinos.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "destino_cell", for: indexPath) as! DestinosTableViewCell
        let destino = self.otrosDestinos[indexPath.item]
        cell.imagenDesto.pin_setImage(from: URL(string: destino.featuredMedia.full.sourceUrl))
        if let encodedString = destino.featuredMedia.full.sourceUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encodedString)
        {
            cell.imagenDesto.pin_setImage(from: url)
        }
        cell.labelDestino.text = destino.title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.nota.gallery != nil){
            return self.nota.gallery.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image_cell", for: indexPath) as! GalleryCollectionViewCell
        if (self.nota.gallery != nil){
            let image = self.nota.gallery[indexPath.item]
            if let encodedString = image.url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encodedString)
            {
                cell.imageGallery.pin_setImage(from: url)
                cell.tag = indexPath.item
            }
        }
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fullscreen_gallery_vc") as! FullScreenViewController
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        popOverVC.setImages(images: self.nota.gallery)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = self.galleryCollectionView.frame.size.width;
        self.currentPage = Int(self.galleryCollectionView.contentOffset.x / pageWidth)
    }
    
    
}
