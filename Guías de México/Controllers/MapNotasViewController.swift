//
//  MapNotasViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/15/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

import GoogleMaps

protocol MapNotasViewControllerDelegate{
    func onMapReady(controller : MapNotasViewController)
}

class MapNotasViewController: UIViewController {
    
    var mapView : GMSMapView!
    var notas : [Post]!
    var delegate : MapNotasViewControllerDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: 19.436351, longitude: -99.1814577, zoom: 8.0)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.view = self.mapView
        
        if((self.delegate) != nil){
            self.delegate.onMapReady(controller: self)
        }
        // Do any additional setup after loading the view.
    }
    
    
    func buildMapa(){
        var firstNotaWithCoordinates : Post!
        for note in self.notas{
            if((note.ubicacion) != nil){
                if(firstNotaWithCoordinates  == nil){
                    firstNotaWithCoordinates = note
                }
                let marker = GMSMarker()
                let position = CLLocationCoordinate2D(latitude: note.ubicacion.lat, longitude: note.ubicacion.lng)
                
                var icon : UIImage!
                
                switch(note.categories[0]){
                    case Constants.POST_CATEGORY_ACTIVIDADES:
                        icon = UIImage(named: "pin_actividades")
                        break;
                    case Constants.POST_CATEGORY_COMER:
                        icon = UIImage(named: "pin_comer")
                    case Constants.POST_CATEGORY_DORMIR:
                        icon = UIImage(named: "pin_dormir")
                    case Constants.POST_CATEGORY_ATRACTIVOS:
                        icon = UIImage(named: "pin_atractivos")
                    default:
                       icon = UIImage(named: "pin")
                       break;
                }
                marker.icon = icon
                marker.position = position
                marker.title = note.title
                marker.snippet = note.title
                marker.groundAnchor = CGPoint(x: 0.5, y: 1)
                marker.appearAnimation = GMSMarkerAnimation.pop
                marker.map = self.mapView
               self.mapView.animate(to: GMSCameraPosition.camera(withTarget: position, zoom: 8.0))
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
