//
//  DestinosViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 10/25/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import MaterialComponents
import GoogleMaps
import PullUpController

class DestinosViewController: UIViewController, DestinosTableViewControllerDelegate {

    @IBOutlet weak var buttonDestinos: UIButton!
    var mapViewController : MapViewController!
    var destinosTableViewController : DestinosTableTableViewController!
    var bottomSheet : MDCBottomSheetController!
    
    var guide : Guia!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeDestinosTableView()
        setupNavigationBar()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mapViewControllerSegue" {
            self.mapViewController = segue.destination as? MapViewController
        }
    }
    
    private func initializeDestinosTableView(){
        if(self.destinosTableViewController == nil){
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            self.destinosTableViewController = storyBoard.instantiateViewController(withIdentifier: "tableview_destinos") as? DestinosTableTableViewController
            self.destinosTableViewController.delegate = self
        }
    }
    
    func setGuide(guide : Guia){
        self.guide = guide
        self.downloadDestinosForGuide()
    }
    
    func downloadDestinosForGuide(){
        let urlGuias = String(format: "https://guiasdigitales.wpengine.com/wp-json/wp/v2/destinos?parent_id=%d&_embedstatus=publish&orderby=title&order=asc&_embed", self.guide.id)
        guard let url = URL(string: urlGuias) else {return}
        URLSession.shared.dataTask(with: url) {
            (data,response,err) in
            guard let data = data else { return }
            do{
                let destinos = try
                   JSONDecoder().decode([Destino].self, from: data)
                self.guide.destinos = destinos
                DispatchQueue.main.sync {
                    self.initializeDestinosTableView()
                    self.destinosTableViewController.setGuia(guide: self.guide)
                    self.mapViewController.setDestinos(destinos: destinos)
                }
            }catch let jsonErr {
                print(jsonErr)
            }
            }.resume()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        self.bottomSheet = MDCBottomSheetController(contentViewController: self.destinosTableViewController)
        self.bottomSheet.hidesBottomBarWhenPushed = false
        self.bottomSheet.isScrimAccessibilityElement = true
        present(self.bottomSheet, animated: true)
    }
    

    
    private func setupNavigationBar(){
//        let image = UIImage(named: "logo-md")?.imageWithInsets(insets: UIEdgeInsets(top: 40, left: 0, bottom: 40, right: 0))
//        let logoImageView = UIImageView(image: image)
//        logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
//        logoImageView.contentMode = .scaleAspectFit
//        navigationItem.titleView = logoImageView
        if(!self.guide.logoAlternativo.isEmpty){
            if let encodedString = self.guide.logoAlternativo.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encodedString)
            {
                let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 64))
                //                logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                logoImageView.pin_setImage(from: url)
                logoImageView.contentMode = .scaleAspectFit
                navigationItem.titleView = logoImageView
            }
        }
    }

    
    @IBAction func onButtonClicked(_ sender: Any) {
        // Present the bottom sheet
        present(self.bottomSheet, animated: true, completion: nil)
    }
    
    func didPressedItem(destino: Destino) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let notasViewController = storyBoard.instantiateViewController(withIdentifier: "vc_notas") as? NotasViewController
        notasViewController!.setDestino(destino: destino, guide: self.guide)
        self.navigationController?.pushViewController(notasViewController!, animated: true)
    }

}
