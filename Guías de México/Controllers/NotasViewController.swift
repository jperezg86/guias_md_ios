//
//  NotasViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/1/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import CarbonKit

class NotasViewController: UIViewController, CarbonTabSwipeNavigationDelegate, PageNotasViewControllerDelegate {
    var destino : Destino!
    var guide : Guia!
//    var guideName : String!
    var groupedPosts : [String : [Post]] = [String:[Post]]()
    var items :  [String] = ["INICIO","ATRACTIVOS","ACTIVIDADES","DORMIR","COMER"]
    
//    var tabControllers : [UIViewController] = [UIViewController]()
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "vc_page_notas") as! PageNotasViewController
        controller.delegate = self
        controller.index = Int(index)
        controller.destino = self.destino
        controller.guia = self.guide
        switch index {
            case 1:
                controller.notas = groupedPosts["atractivos"]
            break;
            case 2:
                controller.notas = groupedPosts["actividades"]
                break;
            case 3:
                controller.notas = groupedPosts["dormir"]
                break;
            case 4:
                controller.notas = groupedPosts["comer"]
                break;
            default:
                controller.notas = destino.posts
        }
        return controller
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    private func setupNavigationBar(){
//        let image = UIImage(named: "logo-md")?.imageWithInsets(insets: UIEdgeInsets(top: 40, left: 0, bottom: 40, right: 0))
        
        
//        let logoImageView = UIImageView(image: image)
//        logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
//        logoImageView.contentMode = .scaleAspectFit
//        navigationItem.titleView = logoImageView
        if(!self.guide.logoAlternativo.isEmpty){
            if let encodedString = self.guide.logoAlternativo.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encodedString)
            {
                let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 64))
                //                logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                logoImageView.pin_setImage(from: url)
                logoImageView.contentMode = .scaleAspectFit
                navigationItem.titleView = logoImageView
            }
        }
    }
    
    
    public func setDestino(destino : Destino, guide : Guia){
        self.destino = destino
        self.guide = guide
        self.downloadNotas(forDestino: destino)
    }
    
    private func downloadNotas(forDestino destino : Destino){
        let urlPosts = String(format: "https://guiasdigitales.wpengine.com/wp-json/wp/v2/posts?parent_id=%d&_embed&per_page=100&status=publish&orderby=title&order=asc", destino.id)
        guard let url = URL(string: urlPosts) else {return}
        URLSession.shared.dataTask(with: url) {
            (data,response,err) in
            guard let data = data else { return }
            do{
                let posts = try
                    JSONDecoder().decode([Post].self, from: data)
                self.destino.posts = posts
                DispatchQueue.main.sync {
                    self.configureTabsController()
                }
            }catch let jsonErr {
                print(jsonErr)
            }
            }.resume()
    }
    
    private func configureTabsController(){
        
        groupedPosts["actividades"] = self.searchPosts(from: self.destino.posts, byCategory: Constants.POST_CATEGORY_ACTIVIDADES)
        groupedPosts["atractivos"] = self.searchPosts(from: self.destino.posts, byCategory: Constants.POST_CATEGORY_ATRACTIVOS)
        groupedPosts["comer"] = self.searchPosts(from: self.destino.posts, byCategory: Constants.POST_CATEGORY_COMER)
        groupedPosts["dormir"] = searchPosts(from: self.destino.posts, byCategory: Constants.POST_CATEGORY_DORMIR)
        
        
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: self.items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.init(rgb: 0xd83489))
        carbonTabSwipeNavigation.setNormalColor(UIColor.init(rgb: 0x69798a), font: UIFont.init(name: "Montserrat-Medium", size: 14)!)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.black, font: UIFont.init(name: "Montserrat-Medium", size: 14)!)
    }
    
    
    func onViewReady(controller : PageNotasViewController) {
//        controller.buildMapa()
        controller.setupBottomSheet()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    /**
     * Devuelve los items Post que encajan en la categoría indicada por aguja
     * de una lista de post original (pajar).
     * @param pajar La lista de elementos donde va a buscar
     * @param aguja El id de la categoría a buscar
     * @return La lista de post que coinciden con esa categoría
     */
    func searchPosts(from pajar : [Post], byCategory category: Int) -> [Post]{
        var postEncontrados : [Post] = [Post]()
        for post : Post in pajar {
            if (post.categories.firstIndex(of: category) != nil) {
                postEncontrados.append(post)
            }
        }
        return postEncontrados
    }
   

}
