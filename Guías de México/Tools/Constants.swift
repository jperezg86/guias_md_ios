//
//  Constants.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/7/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation

struct Constants{
    
    public static let POST_CATEGORY_COMER : Int = 3 ;
    public static let POST_CATEGORY_DORMIR : Int = 4;
    public static let POST_CATEGORY_ATRACTIVOS : Int = 5;
    public static let POST_CATEGORY_ACTIVIDADES : Int = 6;
    
}
