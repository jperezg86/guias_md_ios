//
//  Post.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/7/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation

struct GalleryItem : Decodable{
    var id : Int
    var title : String
    var description : String
    var caption : String
    var url : String
}



class Post : Decodable {
    
    var id : Int
    var title : String
    var excerpt : String
    var body : String
    var featuredMedia : FeaturedMedia!
    var ubicacion : Ubicacion!
    var videoCode : String = ""
    var categories : [Int]!
    var gallery : [GalleryItem]!
    var generales : String!
    
    
    enum CodingKeys : String, CodingKey{
        case id
        case title
        case body = "content"
        case excerpt
        case videoCode = "codigo_de_video"
        case acf
        case categories
        case embedded = "_embedded"
        case rendered
        case ubicacion
        case featuredMedia = "wp:featuredmedia"
        case galeria
        case generales
    }
    
    
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(Int.self, forKey: .id)
        let additionalTitleInfo  = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .title)
        self.title = try additionalTitleInfo.decode(String.self, forKey: .rendered)
        self.categories = try values.decode([Int].self, forKey: .categories)
        let additionalBodyInfo = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .body)
        self.body = try additionalBodyInfo.decode(String.self, forKey: .rendered)
        let additionalExcerptInfo = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .excerpt)
        self.excerpt = try additionalExcerptInfo.decode(String.self, forKey: .rendered)
        let acfProperties = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .acf)
        self.generales = try acfProperties.decode(String.self, forKey: .generales)
        
        do{
            let videoCodeTmp = try acfProperties.decodeIfPresent(String.self, forKey: .videoCode)
            if (videoCodeTmp != nil) {
                self.videoCode = videoCodeTmp!
            }
        }catch let error {
            print(error)
        }
        
        do {
            self.ubicacion = try acfProperties.decodeIfPresent(Ubicacion.self, forKey: .ubicacion)!
        } catch let error {
            print(error)
        }
        
        
        
        do {
            self.gallery  = try acfProperties.decodeIfPresent([GalleryItem].self, forKey: .galeria)!
        } catch let error {
            print(error)
        }
        
        let embeddedPropierties = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .embedded)
        var featuredMediaInfo = try embeddedPropierties.nestedUnkeyedContainer(forKey: .featuredMedia)
        while(!featuredMediaInfo.isAtEnd){
            self.featuredMedia =  try featuredMediaInfo.decode(FeaturedMedia.self)
        }
    }
    
    func getCategoryAsString() -> String {
        var categoryStr : String = ""
        for category in self.categories{
            switch (category){
            case 5:
                categoryStr = "Atractivos"
                break;
            case 6:
                categoryStr = "Actividades"
                break;
            case 4:
                categoryStr = "Dónde Dormir"
                break;
            case 3:
                categoryStr = "Dónde Comer"
                break
            default:
                categoryStr = ""
                break
            }
        }
        return categoryStr
    }
}
