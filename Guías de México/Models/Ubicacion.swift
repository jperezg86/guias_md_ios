//
//  Ubicacion.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/6/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation

struct Ubicacion : Decodable {
    var address : String
    var lat : Double
    var lng : Double
    
    enum CodingKeys : String, CodingKey {
        case address
        case lat
        case lng
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.address = try values.decode(String.self, forKey: .address)
        let latStr = try values.decode(String.self, forKey: .lat)
        self.lat = (latStr as NSString).doubleValue
        let lngStr = try values.decode(String.self, forKey: .lng)
        self.lng = (lngStr as NSString).doubleValue
    }
}
