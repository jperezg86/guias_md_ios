//
//  Destino.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 10/26/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation

class Destino : Decodable {
    var id : Int
    var title : String
    var body : String
    var excerpt : String
    var ubicacion : Ubicacion
    var featuredMedia : FeaturedMedia!
    var posts : [Post]!
    
    
    enum CodingKeys : String, CodingKey{
        case id
        case title
        case body = "content"
        case excerpt
        case acf
        case embedded = "_embedded"
        case rendered
        case ubicacion
        case featuredMedia = "wp:featuredmedia"
    }
    
    
    
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        
        let additionalTitleInfo  = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .title)
        
        title = try additionalTitleInfo.decode(String.self, forKey: .rendered)
        let additionalBodyInfo = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .body)
        
        self.body = try additionalBodyInfo.decode(String.self, forKey: .rendered)
        
        let additionalExcerptInfo = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .excerpt)
        
        self.excerpt = try additionalExcerptInfo.decode(String.self, forKey: .rendered)
        
        let acfProperties = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .acf)
        
        self.ubicacion = try acfProperties.decodeIfPresent(Ubicacion.self, forKey: .ubicacion)!
        
        let embeddedPropierties = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .embedded)
        
        var featuredMediaInfo = try embeddedPropierties.nestedUnkeyedContainer(forKey: .featuredMedia)
        
        while(!featuredMediaInfo.isAtEnd){
            self.featuredMedia =  try featuredMediaInfo.decode(FeaturedMedia.self)
        }
    }
}
