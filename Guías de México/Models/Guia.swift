//
//  Guia.swift
//  Guías de México
//
//  Created by Jose Manuel Perez on 10/23/18.
//  Copyright © 2018 home. All rights reserved.
//

//import Foundation
import UIKit


class Guia : Decodable {
    var id : Int
    var title : String
    var body : String
    var productId : String!
    var featuredMedia : FeaturedMedia!
    var gradientColor : UIColor
    var destinos : [Destino] = [Destino]()
    var logoAlternativo : String
    var published : Bool
    
    enum CodingKeys : String, CodingKey{
        case id
        case title
        case body = "content"
        case acf
        case embedded = "_embedded"
    }
    
    enum AdditionalInfoKeys: String, CodingKey {
        case rendered
        case gradientColor = "gradient_color"
        case featuredMedia = "wp:featuredmedia"
        case logoAlternativo = "logo_alternativo"
        case urlLogoAlternativo = "url"
        case urlFeaturedMedia = "link"
        case mediaDetails = "media_details"
        case available = "is_available"
        case thumbnailUrl = "source_url"
        case sizes
        case full
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        
        let additionalTitleInfo  = try values.nestedContainer(keyedBy: AdditionalInfoKeys.self, forKey: .title)
        title = try additionalTitleInfo.decode(String.self, forKey: .rendered)
        let additionalBodyInfo = try values.nestedContainer(keyedBy: AdditionalInfoKeys.self, forKey: .body)
        self.body = try additionalBodyInfo.decode(String.self, forKey: AdditionalInfoKeys.rendered)
        
        let acfProperties = try values.nestedContainer(keyedBy: AdditionalInfoKeys.self, forKey: .acf)
        let colorStr = try acfProperties.decode(String.self, forKey:
            AdditionalInfoKeys.gradientColor)
        self.gradientColor = UIColor(hex: colorStr)
        let logoAlternativoInfo = try acfProperties.nestedContainer(keyedBy: AdditionalInfoKeys.self, forKey: .logoAlternativo)
        self.logoAlternativo = try logoAlternativoInfo.decode(String.self, forKey: .urlLogoAlternativo)
        
        self.published = try acfProperties.decode(Bool.self, forKey: .available)
        
        let embeddedPropierties = try values.nestedContainer(keyedBy: AdditionalInfoKeys.self, forKey: .embedded)
        
        var featuredMediaInfo = try embeddedPropierties.nestedUnkeyedContainer(forKey: .featuredMedia)
        
        while(!featuredMediaInfo.isAtEnd){
            self.featuredMedia =  try featuredMediaInfo.decode(FeaturedMedia.self)
        }
    }
}
