//
//  CarouselItemCollectionViewCell.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/8/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import MaterialComponents

class CarouselItemCollectionViewCell: MDCCardCollectionCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
