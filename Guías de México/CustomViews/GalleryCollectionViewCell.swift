//
//  GalleryCollectionViewCell.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/22/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageGallery: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
