//
//  DestinosTableViewCell.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 10/26/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import PINRemoteImage

class DestinosTableViewCell: UITableViewCell {

    @IBOutlet weak var imagenDesto: UIImageView!
    @IBOutlet weak var labelDestino: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImageUrl(url: String){
       self.imagenDesto.pin_setImage(from: URL(string: url))
    }
    
}
