//
//  FullScreenGalleryCollectionViewCell.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/26/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class FullScreenGalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var caption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
