//
//  MenuBarCollectionViewCell.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/1/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class MenuBarCollectionViewCell: UICollectionViewCell {
    
   

    @IBOutlet weak var label: UILabel!
    
    override var isHighlighted : Bool {
        didSet {
            label.textColor = isHighlighted ?  UIColor.black : UIColor.gray
        }
    }
    
    override var isSelected: Bool {
        didSet{
            print("Is selected")
            label.textColor = isSelected ?  UIColor.black : UIColor.gray
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setTitle(title : String){
        self.label.text = title.uppercased()
    }

}
