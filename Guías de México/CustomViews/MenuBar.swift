//
//  MenuBar.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/1/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class MenuBar: UIView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var sections : [String] = ["Inicio", "Atractivos", "Actividades", "Dormir","Comer"]
    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = UIColor(rgb: 0xFFFFFF)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sections.count
    }
    
    let cellId = "cellId"
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuBarCollectionViewCell
        cell.backgroundColor = UIColor.white
        
        let width = CGFloat(collectionView.frame.width) / CGFloat(self.sections.count - 1)
        let height = collectionView.frame.height
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
        cell.setTitle(title: self.sections[indexPath.item])
        
        return cell
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
            prepareLayout()
    }
    
    private func prepareLayout(){
        addSubview(collectionView)
        
        let nibCell = UINib(nibName: "MenuBarCollectionViewCell", bundle: nil)
        self.collectionView.register(nibCell, forCellWithReuseIdentifier: cellId)
//        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        addConstraintsWithFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: collectionView)
        backgroundColor = UIColor(rgb: 0x0BB000);
        
        self.collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.bottom)
    }

}
